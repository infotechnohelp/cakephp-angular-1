<?php
$pluginName = 'Angular1';

echo $this->Html->script([
    $pluginName . '.checkRequiredVariables',
    $pluginName . '.container',
    $pluginName . '.object',
    $pluginName . '.setApiDefault',
]);

echo $this->element($pluginName . '.setRoot');

echo $this->Html->script([
    $pluginName . '.Lib/angular.min',
    $pluginName . '.app',
    $pluginName . '.Service/api'
]);
?>