app.service('api', function ($http) {


    function getFullUrl(input, plugin, prefix, extension) {

        if (typeof input === "undefined" || input.length === 0) {
            throw new Error('Empty url');
        }

        var conf = jQuery.extend({}, angular1.api);

        conf.plugin = (typeof plugin === "undefined") ? conf.plugin : plugin;
        conf.prefix = (typeof prefix === "undefined") ? conf.prefix : prefix;
        conf.extension = (typeof extension === "undefined") ? conf.extension : extension;

        var _plugin = (conf.plugin === null ) ? '' : conf.plugin + '/';
        var _prefix = (conf.prefix === null) ? '' : conf.prefix + '/';
        var _extension = (conf.extension === null) ? '' : '.' + conf.extension;

        return conf.root + _plugin + _prefix + input + _extension;
    }

    this.get = function (url, callback, plugin, prefix, extension) {
        var fullUrl = getFullUrl(url, plugin, prefix, extension);
        $http({method: 'GET', url: fullUrl}).then(function (response) {
            callback(response);
        });
    };

    this.post = function (url, data, callback, plugin, prefix, extension) {
        var fullUrl = getFullUrl(url, plugin, prefix, extension);
        $http({method: 'POST', url: fullUrl, data: data}).then(function (response) {
            callback(response);
        });
    };
});