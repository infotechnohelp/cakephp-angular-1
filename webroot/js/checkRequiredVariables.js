var errorEnding = " Used by 'philsweb/cakephp-angular-1'";

if (typeof jQuery === "undefined") {
    throw new Error("jQuery is not set!" + errorEnding);
}

if (typeof angular1container !== "undefined") {
    throw new Error("js variable `angular1container` was already set!" + errorEnding);
}

if (typeof angular1 !== "undefined") {
    throw new Error("js variable `angular1` was already set!" + errorEnding);
}