### Load plugin
`APP/config/bootstrap.php`

```
Plugin::load('Angular1', ['routes' => true]);
```
### Basic implementation
`APP/src/Template/Layout/default.ctp` (or any `.ctp`)

NB!!! Do not forget to load jQuery first

js variables `angular1container` and `angular1` should not be defined
```
<?= $this->element('Angular1.initBasic') ?>
<div data-ng-app="App"></div>
```
You can add now custom angular controllers inside `data-ng-app` element
### Custom controller
`APP/webroot/js/Angular/Controller/Any/index.js`
```
app.controller('any', function ($scope, $rootScope) {
    $scope.init = function () {};
});
```
`... .ctp`
```
<?= $this->Html->script('Angular/Controller/Any/index') ?>
```
`<div data-ng-app="App">`
```
<div ng-controller="any" ng-init="init()"></div>
```
`</div>`
### Service api
#### Set default api parameters
By default, js variable `angular1` has next parameters
```
     angular1.api.root: <?= \Cake\Routing\Router::url('/') ?>
   angular1.api.plugin: null
   angular1.api.prefix: api
angular1.api.extension: null
```
URL formula
```
root + plugin/ + prefix/ + 'Any input' + .extension
```
You can set a new value for any of these parameters
##### Examples
Send to `APP/src/Controller/Api/AnyController.php→ownMethod()` 
###### #1
(`AppRoot/api/any/own-method`)
```
app.controller('any', function ($scope, $rootScope, api) {
        api.get('any/own-method', function (response) {
            var data = response.data;
        });
        api.post('any/own-method', {id:1}, function (response) {
            var data = response.data;
        });
});
```
###### #2
(`AppRoot/api/any/own-method.json`)
```
app.controller('any', function ($scope, $rootScope, api) {
        angular1.api.extension = 'json';
        
        api.get('any/own-method', function (response) {
            var data = response.data;
        });
        
        angular1.api.extension = null;
});
```
###### #3
(`AppRoot/my-plugin/api/any/own-method`)
```
app.controller('any', function ($scope, $rootScope, api) {
        angular1.api.plugin = 'my-plugin';
        
        api.get('any/own-method', function (response) {
            var data = response.data;
        });
        
        angular1.api.plugin = null;
});
```
All `api` service requests have same configs. (js variable `angular1.api`)

It is a good practise to reset them at the end of an Angular controller

In order to change config once (for a current request method only), 
`get()` and `post()` methods have additional optional parameters

`plugin, prefix, extension`
##### Examples
###### #1
(`AppRoot/any/own-method.json`)
```
app.controller('any', function ($scope, $rootScope, api) {
        api.get('any/own-method', function (response) {
            var data = response.data;
        }, undefined, null, 'json');
});
```
###### #2
(`AppRoot/my-plugin/api/any/own-method`)
```
app.controller('any', function ($scope, $rootScope, api) {
        api.get('any/own-method', function (response) {
            var data = response.data;
        }, 'my-plugin');
});
```
NB!!! In case of undefined parameter, method will use a default one from js 
variable `angular1.api`
###### #3
(`AppRoot/my-plugin/api/any/own-method.xml`)
```
app.controller('any', function ($scope, $rootScope, api) {
        angular1.api.plugin = 'my-plugin';
        angular1.api.extension = 'xml';
        
        api.get('any/own-method', function (response) {
            var data = response.data;
        });
        
        angular1.api.plugin = null;
        angular1.api.extension = null;
});
```
In case of empty url value (`''`) a console error is thrown `Empty url`
